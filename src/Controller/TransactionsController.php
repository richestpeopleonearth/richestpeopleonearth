<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Cache\Cache;

class TransactionsController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    public function payment()
    {
        $amount = $this->request->query('amount') * 100;
        $environment = ($_SERVER['HTTP_HOST'] != 'dev.richestpeopleonearth.com') ? 'production' : 'sandbox';

        $this->set(compact('amount', 'environment'));
        $this->set('_serialize', ['amount', 'environment']);
    }

    public function paymentAmount()
    {
        if ($this->request->is('post')) {
            if (empty($this->request->data('amount'))) {
                $this->Flash->error(__('Please enter an amount no less than $1'));
            } else {
                return $this->redirect(['_name' => 'payment', 'amount' => $this->request->data('amount')]);
            }
        }
    }

    public function confirmPayment()
    {
        if ($this->request->is('ajax')) {
            if ($this->request->query()['payment']['state'] == 'approved') {       
                $usersTable = TableRegistry::get('Users');
                if(TableRegistry::get('Transactions')->addTransactionByUserId($this->request->query(), $this->Auth->user('id'))) {
                    $usersTable->addWealthByUserId($this->request->query()['payment']['transactions'][0]['amount']['total'], $this->Auth->user('id'));

                    // BREAK CACHE
                    Cache::clear(false);

                    $usersTable->checkNewRank($this->Auth->user('id'));
                }
            }
        }
    }
}
