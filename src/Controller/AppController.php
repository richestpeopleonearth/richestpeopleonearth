<?php
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

class AppController extends Controller
{
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Security');
        $this->loadComponent('Csrf');
        $this->loadComponent('Auth', [
            'loginRedirect' => ['_name' => 'home'],
            'logoutRedirect' => ['_name' => 'home']
        ]);
    }

    public function beforeFilter(Event $event)
    {
        // Which pages are NOT behind the login wall
        $this->Auth->allow([
            'home',
            'register',
            'search',
            'login',
            'verify',
            'about',
            'contact',
            'terms',
            'privacy',
            'share'
        ]);

        // Disable security on stripe form (crsf kills stripe form)
        $this->Security->setConfig('unlockedActions', ['payment', 'register']);
    }

    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }
}
