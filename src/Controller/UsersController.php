<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;
use Cake\Cache\Cache;

class UsersController extends AppController
{
    public function home()
    {
        $currentUser = null;
        if ($this->Auth->user()) {
            if (($currentUser = Cache::read('currentUser' . $this->Auth->user('id'))) === false) {
                $currentUser = $this->Users->get(
                    $this->Auth->user('id'),
                    ['contain' => ['Countries']]
                );
                Cache::write('currentUser' . $this->Auth->user('id'), $currentUser);
            }  
        }

        $this->paginate = [
            'contain' => ['Countries'],
            'limit' => 100,
            'order' => [
                'Users.wealth' => 'desc'
            ]
        ];

        if (($users = Cache::read('users')) === false) {
            $users = $this->paginate($this->Users);
            Cache::write('users', $users);
        }       

        $this->set(compact('users', 'currentUser'));
        $this->set('_serialize', ['users', 'currentUser']);
    }

    public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                if (empty($user['hash'])) {
                    $this->Auth->setUser($user);
                    return $this->redirect($this->Auth->redirectUrl());
                }
                else
                {
                    $this->Flash->error(__('Please click the Verification Link in your account verification email.'));
                }
            }
            else
            {
                $this->Flash->error(__('Invalid username or password, try again'));
            }
        }
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    public function profile()
    {
        $user = $this->Users->get($this->Auth->user('id'), [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'home']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $genders = $this->Users->Genders->find('list');
        $countries = $this->Users->Countries->find('list');
        $this->set(compact('user', 'genders', 'countries'));
        $this->set('_serialize', ['user', 'genders', 'countries']);
    }

    public function register()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Registration Successful! Please click the Verification Link in your account verification email.'));

                return $this->redirect(['action' => 'home']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $genders = $this->Users->Genders->find('list');
        $countries = $this->Users->Countries->find('list');
        $this->set(compact('user', 'genders', 'countries'));
        $this->set('_serialize', ['user', 'genders', 'countries']);
    }

    public function search()
    {
        // TODO
        // if post
        // if name like
        // if country =
        $this->paginate = [
            'contain' => ['Genders', 'Countries'],
            'limit' => 100,
            'order' => [
                'Users.wealth' => 'desc'
            ]
        ];
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    public function verify()
    {
        if ($this->request->query('hash')) {
            $user = $this->Users->find()->where(['hash' => $this->request->query('hash')])->first();

            if ($user)
            {
                $user->hash = null;
                unset($user->password);
                if ($this->Users->save($user)) {
                    $this->Flash->success(__('Your email has been verified'));
                    return $this->redirect(Router::url(['_name' => 'login']));
                }
            }
        }

        $this->Flash->error(__('Your email has NOT been verified. Please try again.'));
        return $this->redirect(Router::url(['_name' => 'login']));
    }

    public function resetPassword()
    {
        // if ($this->request->query('hash')) {
        //     $user = $this->Users->find()->where(['hash' => $this->request->query('hash')])->first();

        //     if ($user)
        //     {
        //         $user->hash = null;
        //         unset($user->password);
        //         if ($this->Users->save($user)) {
        //             $this->Flash->success(__('Your email has been verified'));
        //             return $this->redirect(Router::url(['_name' => 'login']));
        //         }
        //     }
        // }

        // $this->Flash->error(__('Your email has NOT been verified. Please try again.'));
        // return $this->redirect(Router::url(['_name' => 'login']));
    }
}