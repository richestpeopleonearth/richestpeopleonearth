<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Cache\Cache;

/**
 * User Entity
 *
 * @property int $id
 * @property string $full_name
 * @property string $email
 * @property string $password
 * @property int $gender
 * @property int $country
 * @property int $score
 */
class User extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    protected $_hidden = [
        'password'
    ];

    protected function _setPassword($password)
    {
        if (strlen($password) > 0) {
            return (new DefaultPasswordHasher)->hash($password);
        }
    }

    protected function _getRank()
    {
        if (($rank = Cache::read('rank' . $this->id)) === false) {
            $query = TableRegistry::get('Users')->find();
            $query->select(['count' => $query->func()->count('*')]);
            $query->where(['wealth >=' => $this->wealth]);
            $rank = $query->first()->count;
            Cache::write('rank' . $this->id, $rank);
        }  

        return $rank;
    }
}
