<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\Mailer\Email;

/**
 * Transactions Model
 *
 * @method \App\Model\Entity\Transaction get($primaryKey, $options = [])
 * @method \App\Model\Entity\Transaction newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Transaction[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Transaction|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Transaction patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Transaction[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Transaction findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TransactionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('transactions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('payment_method');

        $validator
            ->allowEmpty('amount');

        return $validator;
    }

    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['paypal_id']));

        return $rules;
    }

    public function addTransactionByUserId($transactionData, $userId)
    {
        $transaction = $this->newEntity();
        $transaction->user_id = $userId;
        $transaction->payment_method = 'Stripe';
        $transaction->amount = $transactionData['payment']['transactions'][0]['amount']['total'];
        $transaction->response = json_encode($transactionData);
        $transaction->paypal_id = $transactionData['payment']['id'];
        return $this->save($transaction);
    }

    public function afterSave(Event $event, Entity $entity)
    {
        $user = TableRegistry::get('Users')->findById($entity->user_id)->first();

        if ($entity->isNew()) {
            $email = new Email();
            $email->from(['noreply@richestpeopleonearth.com' => 'Richest People On Earth'])
                ->to($user->username)
                // ->to('richestpeopleonearthcom@gmail.com')
                ->subject('Richest People On Earth: Payment Successful')
                ->emailFormat('html')
                ->template('payment_confirmation', 'default')
                ->viewVars([
                    'display_name' => $user->display_name,
                    'amount' => $entity->amount,
                    'rank' => $user->rank
                ])
                ->send();
        }
    }
}