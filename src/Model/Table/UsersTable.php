<?php
namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\Entity;
use Cake\Validation\Validator;
use Cake\Utility\Security;
use Cake\Mailer\Email;
use Cake\Event\Event;
use Cake\Cache\Cache;

class UsersTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Genders', [
            'foreignKey' => 'gender_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Countries', [
            'foreignKey' => 'country_id',
            'joinType' => 'INNER'
        ]);

        $this->hasMany('Transactions', [
            'foreignKey' => 'user_id'
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('display_name', 'create')
            ->notEmpty('display_name', 'A display name is required')
            ->add('title', [
                'length' => [
                    'rule' => ['maxLength', 28],
                    'message' => 'Your display name cannot be longer than 28 characters',
                ]
            ]);

        $validator
            ->email('username')
            ->requirePresence('username', 'create')
            ->notEmpty('username', 'A username is required');

        $validator
            ->requirePresence('password', 'create')
            ->notEmpty('password', 'A password is required');

        // $validator
        //     ->requirePresence('gender_id', 'create')
        //     ->notEmpty('gender_id', 'A gender is required');

        $validator
            ->requirePresence('country_id', 'create')
            ->notEmpty('country_id', 'A country is required');

        $validator
            ->allowEmpty('wealth');

        return $validator;
    }

    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));

        return $rules;
    }

    public function beforeSave(Event $event, Entity $entity)
    {
        if ($entity->isNew()) {
            $entity->set('hash', Security::hash($entity->username));
        }
    }

    public function afterSave(Event $event, Entity $entity)
    {
        // BREAK CACHE
        Cache::clear(false);

        if ($entity->isNew()) {
            $email = new Email();
            $email->from(['noreply@richestpeopleonearth.com' => 'Richest People on Earth'])
                ->to($entity->username)
                // ->to('richestpeopleonearthcom@gmail.com')
                ->subject('Richest People on Earth: Account Verification')
                ->emailFormat('html')
                ->template('verify', 'default')
                ->viewVars([
                    'hash' => $entity->hash,
                    'display_name' => $entity->display_name
                ])
                ->send();
        }
    }

    public function addWealthByUserId($wealth, $userId)
    {
        $user = $this->get($userId);
        $user->wealth += $wealth;
        return $this->save($user);
    }

    public function checkNewRank($userId)
    {
        $user = $this->get($userId);

        if ($user->rank == 1) {
            $rankTwoUser = $this->find('all')
                ->order(['wealth' => 'DESC'])
                ->offset(1)
                ->first();

            $email = new Email();
            $email->from(['noreply@richestpeopleonearth.com' => 'Richest People on Earth'])
                ->to($rankTwoUser->username)
                // ->to('richestpeopleonearthcom@gmail.com')
                ->subject('Richest People on Earth: New Rank #1')
                ->emailFormat('html')
                ->template('new_rank_one', 'default')
                ->viewVars([
                    'display_name' => $rankTwoUser->display_name,
                    'new_rank_one_display_name' => $user->display_name
                ])
                ->send();
        }
    }
}