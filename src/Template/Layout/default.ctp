<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?= $this->fetch('title') ?></title>
        <?= $this->Html->meta('icon') ?>

        <meta itemprop="name" content="<?= $this->fetch('title') ?>" />
        <meta itemprop="image" content="https://www.richestpeopleonearth.com/img/general/share-logo.png" />
        <meta itemprop="description" content="Join the Richest People on Earth!" />

        <meta name="description" content="Join the Richest People on Earth!" />
        <meta name="author" content="<?= $this->fetch('title') ?>" />

        <meta property="og:title" content="<?= $this->fetch('title') ?>" />
        <meta property="og:type" content="article" />
        <meta property="og:description" content="Join The Richest People on Earth!" />
        <meta property="og:image" content="https://www.richestpeopleonearth.com/img/general/share-logo.png" />
        <meta property="og:image:width" content="167" />
        <meta property="og:image:height" content="178" />
        <meta property="og:url" content="https://www.richestpeopleonearth.com/" />
        <meta property="og:site_name" content="<?= $this->fetch('title') ?>" />
        <meta property="fb:app_id" content="122003975130974" />

        <meta name="twitter:site" content="richestpeopleonearth">
        <meta name="twitter:url" content="https://www.richestpeopleonearth.com/">
        <meta name="twitter:title" content="<?= $this->fetch('title') ?>">
        <meta name="twitter:description" content="Join the Richest People on Earth!">
        <meta name="twitter:image" content="https://www.richestpeopleonearth.com/img/general/share-logo-400.png">
        <meta name="twitter:card" content="summary">

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-104486012-1', 'auto');
          ga('send', 'pageview');
        </script>

        <?= $this->Html->script('app.js') ?>
        <?= $this->Html->css('app.css') ?>
        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
    </head>
    
    <body>
        <div class="row">
            <div class="small-12 columns nopadding">
                <nav>
                    <div class="top-bar">
                        <div class="top-bar-left">
                            <ul class="dropdown menu" data-dropdown-menu>
                                <li><a href="/" class="the-crown" title="Richest People on Earth Home"><?= $this->Html->image('/img/general/logo.png', ['alt' => 'Richest People on Earth', 'width' => 50, 'height' => 53]) ?></a></li>
                                <li class="menu-text"><a href="/" class="the-crown" title="Richest People on Earth Home"><h1>Richest People on Earth</h1></a></li>
                            </ul>
                        </div>
                        <div class="top-bar-right">
                            <div class="row main-menu">
                                <ul class="dropdown menu" data-dropdown-menu>
                                    <?php if($this->request->session()->read('Auth.User.id')): ?>
                                    <li><b><?= $this->Html->link('Increase Wealth', ['_name' => 'payment-amount'], ['class' => 'increase-wealth border-right-none']) ?></b></li>
                                    <li><b><?= $this->Html->link('Profile', ['_name' => 'profile'], ['class' => 'border-right-none']) ?></b></li>
                                    <li><b><?= $this->Html->link('Logout', ['_name' => 'logout']) ?></b></li>
                                    <?php else: ?>
                                    <li><b><?= $this->Html->link('Create a Free Account', ['_name' => 'register'], ['class' => 'join border-right-none']) ?></b></li>
                                    <li><b><?= $this->Html->link('Login', ['_name' => 'login']) ?></b></li>
                                    <?php endif; ?>
                                    <!-- Need to build the search page -->
                                    <!-- <li><b><?= $this->Html->link('Search', ['_name' => 'search']) ?></b></li> -->
                                </ul>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>

        <?= $this->Flash->render() ?>
        <?= $this->fetch('content') ?>

        <div class="row">
            <div class="small-12 columns nopadding">
                <footer>
                    <div class="top-bar">
                        <div class="top-bar-left">
                            <ul class="dropdown menu" data-dropdown-menu>
                                <li><p class="text-center">© 2017, www.richestpeopleonearth.com</p></li>
                            </ul>
                        </div>
                        <div class="top-bar-right">
                            <ul class="dropdown menu" data-dropdown-menu>
                                <li><?= $this->Html->link('About', ['_name' => 'about']) ?></li>
                                <li><?= $this->Html->link('Contact Us', ['_name' => 'contact']) ?></li>
                                <li><?= $this->Html->link('Terms and Conditions', ['_name' => 'terms']) ?></li>
                                <li><?= $this->Html->link('Privacy Notice', ['_name' => 'privacy']) ?></li>
                                <li class="hidden"><a href="www.icondrawer.com" style="font-size: 1px;">www.icondrawer.com</a></li>
                            </ul>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </body>
</html>