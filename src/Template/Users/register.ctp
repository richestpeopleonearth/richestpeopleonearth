<?= $this->assign('title', 'Register') ?>

<div class="register form">
    <div class="row">
        <div class="small-12 large-6 large-offset-3 columns">
        <?= $this->Form->create($user) ?>
            <div class="row">
                <div class="small-l2 columns">
                    <h3><?= __('Please complete the registration form') ?></h3>
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    <?= $this->Form->control('display_name') ?>
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    <?= $this->Form->control('username', ['label' => 'Email']) ?>
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    <?= $this->Form->control('password') ?>
                </div>
            </div>
<!--             <div class="row">
                <div class="small-12 columns">
                    <?= $this->Form->control('gender_id', ['options' => $genders, 'empty' => true]) ?>
                </div>
            </div> -->
            <div class="row">
                <div class="small-12 columns">
                    <?= $this->Form->control('country_id', ['options' => $countries, 'empty' => true]) ?>
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    <p>By clicking "Register" you are agreeing to our <?= $this->Html->link('terms and conditions', ['_name' => 'terms', 'target' => '_blank']) ?>.</p>
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    <?= $this->Form->button(__('Register'), ['class' => 'large button']); ?>
                </div>
            </div>
        <?= $this->Form->end() ?>
        </div>
    </div>
</div>