<?= $this->assign('title', 'Reset Password') ?>

<div class="reset-password form">
	<div class="row">
		<div class="small-12 large-6 large-offset-3 columns">
		<?= $this->Form->create() ?>
			<div class="row">
				<div class="small-l2 columns">
					<h3><?= __('Reset password') ?></h3>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
			        <?= $this->Form->control('username') ?>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<?= $this->Form->button(__('Reset Password'), ['class' => 'large button']); ?>
				</div>
			</div>
		<?= $this->Form->end() ?>
		</div>
	</div>
</div>