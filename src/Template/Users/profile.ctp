<?= $this->assign('title', 'Profile') ?>

<div class="profile form">
    <div class="row">
        <div class="small-12 large-6 large-offset-3 columns">
        <?= $this->Form->create($user) ?>
            <div class="row">
                <div class="small-l2 columns">
                    <h3><?= __($user->display_name . '\'s Profile (Rank #' . $user->rank . ')') ?></h3>
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    <?= $this->Form->control('display_name', ['disabled' => true]) ?>
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    <?= $this->Form->control('username', ['disabled' => true, 'label' => 'Email']) ?>
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    <?= $this->Form->control('password') ?>
                </div>
            </div>
<!--             <div class="row">
                <div class="small-12 columns">
                    <?= $this->Form->control('gender_id', ['options' => $genders, 'empty' => true, 'disabled' => true]) ?>
                </div>
            </div> -->
            <div class="row">
                <div class="small-12 columns">
                    <?= $this->Form->control('country_id', ['options' => $countries, 'empty' => true, 'disabled' => true]) ?>
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    <?= $this->Form->button(__('Update Profile'), ['class' => 'large button']); ?>
                </div>
            </div>
        <?= $this->Form->end() ?>
        </div>
    </div>
</div>