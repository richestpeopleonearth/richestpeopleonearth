<?= $this->assign('title', 'Richest People on Earth') ?>

<div class="search index small-12 columns content ">
<!--     <div class="row">
        <div class="small-12">
            <form method="GET" action="/search">
                <input type="text" name="name">
                <select name="country">
                    <option value="1">1</option>
                </select>
                <button type="submit">Submit</button>
            </form>
        </div>
    </div> -->
    <div class="row">
        <div class="small-12 columns">
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th scope="col"># Rank</th>
                        <th scope="col">Name</th>
                        <th scope="col">Country</th>
                        <th scope="col">Wealth</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($users as $rank => $user): ?>
                    <tr>
                        <td><?= h($user->rank) ?></td>
                        <td><?= h($user->display_name) ?></td>
                        <td><?= $this->Html->image($user->country->image_path, ['alt' => $user->country->name]) ?> <?= $user->country->name ?></td>
                        <td>$<?= h(!empty($user->wealth)?$user->wealth:0) ?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="small-12">
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                </ul>
                <p><?= $this->Paginator->counter() ?></p>
            </div>
        </div>
    </div>
</div>