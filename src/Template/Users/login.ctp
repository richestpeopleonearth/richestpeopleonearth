<?= $this->assign('title', 'Login') ?>

<div class="login form">
	<div class="row">
		<div class="small-12 large-6 large-offset-3 columns">
		<?= $this->Form->create() ?>
			<div class="row">
				<div class="small-l2 columns">
					<h3><?= __('Please login using your Email and Password') ?></h3>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
			        <?= $this->Form->control('username', ['label' => 'Email']) ?>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
			        <?= $this->Form->control('password') ?>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<?= $this->Form->button(__('Login'), ['class' => 'large button']); ?>
				</div>
			</div>
		<?= $this->Form->end() ?>
		</div>
	</div>
</div>