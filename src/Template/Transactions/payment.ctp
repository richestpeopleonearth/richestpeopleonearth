<?= $this->assign('title', 'Payment') ?>

<div class="payment form">
    <?php if((!empty($amount) && $amount >= 100) || $this->request->is('ajax')): ?>
    <div class="row">
        <div class="small-12 large-6 large-offset-3 columns">
            <div class="row payment-header">
                <div class="small-l2 columns">
                    <h3><?= __('Payment Options') ?></h3>
                </div>
                <div class="small-l2 columns">
                    <p><?= __('Increase Wealth by $' . $amount / 100) ?></p>
                </div>
            </div>
            <div class="row paypal-section">
                <div class="small-l2 columns">
                    <div id="paypal-button"></div>
                    <script src="https://www.paypalobjects.com/api/checkout.js"></script>
                    <script>
                        paypal.Button.render({
                            env: '<?= $environment ?>',
                            client: {
                                sandbox:    'AWE6Ejz3crSLeUL5D8O2JTGBti5Vr3Yr_JqXgyqY8pxWw_KTXoq79nvVc53hUx_K3b-zqRZUdaLFCwtv',
                                production: 'AaYPb65GvPwSkXNR7qsnoQcjb8PfJ7d__lMReNLgjerjenjGuHZUh4DIcOw9NF1jN8jYZXpFUpFtbvTu'
                            },
                            commit: true, // Show a 'Pay Now' button
                            payment: function(data, actions) {
                                var url = new URL(window.location.href);
                                var param = url.searchParams.get('amount');

                                return actions.payment.create({
                                    payment: {
                                        transactions: [
                                            {
                                                amount: { total: param, currency: 'USD' }
                                            }
                                        ]
                                    }
                                });
                            },
                            onAuthorize: function(data, actions) {
                                return actions.payment.execute().then(function(payment) {
                                    $.ajax({
                                        url: 'confirm-payment',
                                        data: {data: 'data', actions: 'actions', payment: payment},
                                        type: 'GET',
                                        beforeSend: function() {
                                            $('.paypal-loader').show();
                                        },
                                        success: function(result) {
                                            $('.paypal-section').hide();
                                            $('.payment-header').hide();
                                            $('.paypal-success').show();
                                        },
                                        complete: function() {
                                            $('.paypal-loader').hide();
                                        },
                                    });

                                });
                            }
                        }, '#paypal-button');
                    </script>
                </div>
            </div>
            <div class="row paypal-loader hidden">
                <div class="small-12 columns">
                    <?= $this->Html->image('/img/general/loader.gif', ['alt' => 'Loading...']) ?>
                </div>
            </div>
            <div class="row paypal-success hidden">
                <div class="small-12 columns">
                    <h3>Congratulations, your payment was successful!</h3>
                    <a href="/"><button type="button" class="success button">Click Here</button></a>
                </div>
            </div>
        </div>
    </div>

    <?php else: ?>
    <div class="row">
        <div class="small-12 large-6 large-offset-3 columns">
            <div class="row">
                <div class="small-12 columns">
                    <p>First you need to select a payment amount. <?= $this->Html->link('Click here', ['_name' => 'payment-amount']) ?>.</p>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
</div>

