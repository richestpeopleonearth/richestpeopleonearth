<?= $this->assign('title', 'Payment Amount') ?>

<div class="payment-amount form">
    <div class="row">
        <div class="small-12 large-6 large-offset-3 columns">
        <?= $this->Form->create() ?>
            <div class="row">
                <div class="small-l2 columns">
                    <h3><?= __('How many $ do you want to increase your wealth by ?') ?></h3>
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                <?= $this->Form->control('amount', [
                    'type' => 'number',
                    'min' => 1,
                    'step' => 1,
                    'placeholder' => '100',
                    'label' => 'Amount'
                ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    <?= $this->Form->button(__('Proceed to Payment'), ['class' => 'large button']); ?>
                </div>
            </div>
        <?= $this->Form->end() ?>
        </div>
    </div>
</div>