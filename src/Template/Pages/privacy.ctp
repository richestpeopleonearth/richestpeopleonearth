<?= $this->assign('title', 'Privacy Notice') ?>

<div class="privacy row">
	<div class="small-12 columns">
	    <h2>Privacy Notice</h2>
	</div>
	<div class="small-12 columns">
	    <h3>We take your privacy and the security of your personal information seriously.</h3>

		<p>We have implemented reasonable security safeguards to protect the personal information that you provide. For example sensitive data is protected by SSL encryption when it is exchanged between your web browser and our website.</p>

		<p>We regularly monitor our systems for possible vulnerabilities and attacks. As no system is perfect we cannot guarantee that information may not be accessed, disclosed, altered or destroyed by breach of any of our physical, technical or managerial safeguards.</p>

		<p>Please note that any e-mail you send to us is not encrypted and may be monitored by us. Please do not send us sensitive or confidential personal information by means of e-mail.</p>
	</div>
</div>