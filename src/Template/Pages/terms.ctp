<?= $this->assign('title', 'Terms') ?>

<div class="terms row">
	<div class="small-12 columns">
		<div class="row">
			<div class="small-12 columns">
			    <h2>Terms and Conditions</h2>
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns">
				<div class="row">
					<div class="small-12 columns">
					    <h3>GENERAL TERMS OF USE</h3>
					</div>
				</div>
				<div class="row">
					<div class="small-12 columns">
						<p>Access and use of this website demonstrates your acceptance of and agreement to our terms and conditions. These <?= $this->Html->link('Terms and Conditions', ['_name' => 'terms']) ?> along with our <?= $this->Html->link('Privacy Notice', ['_name' => 'privacy']) ?> is applicable to anyone visiting or making use of our website, mobile application or service.</p>
						<p>We reserve the right to modify or discontinue, temporarily or permanently, the information and services provided (or any part thereof) at any time, and from time to time, with or without prior notice. Any person accessing this website agrees that we the site owners shall not be liable to visitors or to any third party for any suspension, modification, or termination of the information and services provided on this website.</p>
						<p>Any material or software that you download from this website is done so at your sole discretion and risk, and that you will be solely responsible for any damage or harm caused to your network connection(s), computer or computer-related software and equipment, system loss or data that results from your visit to this site and/or the download of any data or information you made from the site.</p>
						<p>The wealth purchased on www.richestpeopleonearth.com is virtual content and in no way resembles a currency or balance of which can be withdrawn or refuned in any way after purchase.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns">
				<div class="row">
					<div class="small-12 columns">
						<h3>EXCLUSION OF LIABILITY</h3>
					</div>
				</div>
				<div class="row">
					<div class="small-12 columns">
						<p>While we have made every reasonable effort to ensure the accuracy and correctness of the information contained in this site and the sites linked to this site, the companies will not, under any circumstances, be liable or responsible for any loss or damage whatsoever sustained or incurred arising from any use of, or reliance on, such information whatsoever, and arising from any cause whatsoever, including, without limitation, negligence.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns">
				<div class="row">
					<div class="small-12 columns">
						<h3>SECURITY POLICY</h3>
					</div>
				</div>
				<div class="row">
					<div class="small-12 columns">
						<p>This site has security measures in place to protect the loss, misuse and alteration of the information under our control.</p>
						<p>For site security purposes and to ensure that this service remains available to all users, this website employs software programs to monitor network traffic to identify unauthorized attempts to upload or change information, or otherwise cause damage.</p>
						<p>Unauthorized attempts to upload information or change information on this service are strictly prohibited and may be punishable under the Computer Fraud and Abuse Act of 1986 and the National Information Infrastructure Protection Act.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns">
				<div class="row">
					<div class="small-12 columns">
						<h3>REFUNDS</h3>
					</div>
				</div>
				<div class="row">
					<div class="small-12 columns">
						<p>We do not offer any types of refunds on any payments made through <a href="/">www.richestpeopleonearth.com</a> or any payments made to the owners of <a href="/">www.richestpeopleonearth.com</a> using any types of payment methods.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>