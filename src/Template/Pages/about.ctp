<?= $this->assign('title', 'About') ?>

<div class="about row">
	<div class="small-12 columns">
		<div class="row">
	    	<div class="small-12 columns">	
		    	<h2>About Us</h2>
		    </div>
		</div>
		<div class="row">
			<div class="small-12 columns">
				<p>Richest People on Earth was founded in 2017. This global platform was created to allow the super wealthy to show the rest of the world just how rich they are. All people from all the countries in the world have access to participate, making this a level playground for the worlds most wealthy. At Richest People on Earth we believe that being wealthy means little if you cannot show off from time to time.</p>
			</div>
		</div>
	</div>
</div>