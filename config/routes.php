<?php
use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {
	// Users
	$routes->connect('/', ['controller' => 'Users', 'action' => 'home'], ['_name' => 'home']);
	$routes->connect('/profile', ['controller' => 'Users', 'action' => 'profile'], ['_name' => 'profile']);
	$routes->connect('/register', ['controller' => 'Users', 'action' => 'register'], ['_name' => 'register']);
	$routes->connect('/search', ['controller' => 'Users', 'action' => 'search'], ['_name' => 'search']);
	$routes->connect('/login', ['controller' => 'Users', 'action' => 'login'], ['_name' => 'login']);
	$routes->connect('/logout', ['controller' => 'Users', 'action' => 'logout'], ['_name' => 'logout']);
	$routes->connect('/verify', ['controller' => 'Users', 'action' => 'verify'], ['_name' => 'verify']);

	// Transactions
	$routes->connect('/payment-amount', ['controller' => 'Transactions', 'action' => 'paymentAmount'], ['_name' => 'payment-amount']);
	$routes->connect('/payment', ['controller' => 'Transactions', 'action' => 'payment'], ['_name' => 'payment']);
	$routes->connect('/confirm-payment', ['controller' => 'Transactions', 'action' => 'confirmPayment'], ['_name' => 'confirm-payment']);

	// Pages
	$routes->connect('/reset-password', ['controller' => 'Pages', 'action' => 'reset-password'], ['_name' => 'reset-password']);
	$routes->connect('/about', ['controller' => 'Pages', 'action' => 'about'], ['_name' => 'about']);
	$routes->connect('/contact', ['controller' => 'Pages', 'action' => 'contact'], ['_name' => 'contact']);
	$routes->connect('/terms', ['controller' => 'Pages', 'action' => 'terms'], ['_name' => 'terms']);
	$routes->connect('/privacy', ['controller' => 'Pages', 'action' => 'privacy'], ['_name' => 'privacy']);
	
	// disabled
	// $routes->connect('/share', ['controller' => 'Pages', 'action' => 'share'], ['_name' => 'share']);

    $routes->fallbacks(DashedRoute::class);
});

Plugin::routes();