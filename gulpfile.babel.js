'use strict';

import plugins       from 'gulp-load-plugins';
import yargs         from 'yargs';
import gulp          from 'gulp';
import rimraf        from 'rimraf';
import yaml          from 'js-yaml';
import fs            from 'fs';
import webpackStream from 'webpack-stream';
import webpack2      from 'webpack';
import named         from 'vinyl-named';

const $ = plugins();
const PRODUCTION = !!(yargs.argv.production);
const { COMPATIBILITY, UNCSS_OPTIONS, PATHS } = loadConfig();

let webpackConfig = {
  module: {
    rules: [
      {
        test: /.js$/,
        use: [
          {
            loader: 'babel-loader'
          }
        ]
      }
    ]
  }
}

gulp.task('build', gulp.series(clean, gulp.parallel(sass, javascript)));
gulp.task('default', gulp.series('build', watch));

function loadConfig() {
  let ymlFile = fs.readFileSync('config.yml', 'utf8');
  return yaml.load(ymlFile);
}

function clean(done) {
  rimraf('webroot/css/app.css', done);
  rimraf('webroot/js/app.js', done);
}

function sass() {
  return gulp.src(PATHS.sass)
    .pipe($.concat('app.css'))
    .pipe($.sass({
      includePaths: PATHS.sass_lib
    })
    .on('error', $.sass.logError))
    .pipe($.if(PRODUCTION, $.cleanCss({ compatibility: 'ie9' })))
    .pipe($.if(!PRODUCTION, $.sourcemaps.write()))
    .pipe(gulp.dest(PATHS.webroot + '/css'))
}

function javascript() {
  return gulp.src(PATHS.javascript)
    .pipe($.concat('app.js'))
    .pipe(named())
    .pipe($.sourcemaps.init())
    .pipe(webpackStream(webpackConfig, webpack2))
    .pipe($.if(PRODUCTION, $.uglify()
      .on('error', e => { console.log(e); })
    ))
    .pipe($.if(!PRODUCTION, $.sourcemaps.write()))
    .pipe(gulp.dest(PATHS.webroot + '/js'));
}

function watch() {
  gulp.watch('assets/scss/**/*.scss').on('all', sass);
  gulp.watch('assets/js/**/*.js').on('all', javascript);
}